"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    time = 0
    x = control_dist_km
    while(x > 0): #x is the control km, if x > 0, keep calculate the time
        if x >= 1000:
            if x == 1000:
                time = time + 400/28
                x = 600
            elif (x - 1000) < (1000 * 0.2): # if control distance not over %20 of brevet km, just regard it as brevet km.
                x = 1000
            else:
                time = time + (x-1000)/26
                x = 1000
        elif x >= 600:
            if x == 600:
                time = time + 200/30
                x = 400
            elif (x - 600) < (600*0.2): # if control distance not over %20 of brevet km, just regard it as brevet km
                x = 600
            else:
                time = time + (x - 600)/28
                x = 600
        elif x >= 400:
            if x == 400:
                time = time + 200/32
                x = 200
            elif (x - 400) < (400*0.2): # if control distance not over %20 of brevet km, just regard it as brevet km
                x = 400
            else:
                time = time + (x - 400)/30
                x = 400
        elif x >= 200:
            if x == 200:
                time = time + 200/34
                x = 0
            elif(x - 200)<(200*0.2): # if control distance not over %20 of brevet km, just regard it as brevet km
                x = 200
            else:
                time = time + (x - 200)/32
                x = 200
        else:
            time = time + x / 34
            x = 0
    hour = int(time) # get hour
    minute = round((time - hour)*60) #minutes = 60(minute) * 0.() （the decimal fraction）
    brevet_time = arrow.get(brevet_start_time) #brevet_time store the start time use arrow
    open = brevet_time.shift(hours = hour, minutes = minute) # change the time, get open time

    return open.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):

    time = 0
    x = control_dist_km
    if x == 200 or (x > 200 and x <= 240):
        time = 200/15 + 1/6
        x = 0
    elif x == 400 or (x > 400 and x <= 480):
        time = 400/15 + 1/3
        x = 0
    else:
        while(x > 0): #x is the control km, if x > 0, keep calculate the time
            if x >= 1000:
                if x == 1000:
                    time = time + 400/11.428
                    x = 600
                elif (x - 1000) < (1000 * 0.2): # if control distance not over %20 of brevet km, just regard it as brevet km
                    x = 1000
                else:
                    time = time + (x-1000)/13.333
                    x = 1000
            elif x >= 600:
                if x == 600:
                    time = time + 200/15
                    x = 400
                elif (x - 600) < (600*0.2): # if control distance not over %20 of brevet km, just regard it as brevet km
                    x = 600
                else:
                    time = time + (x - 600)/11.428
                    x = 600
            else:
                time = time + x/15
                x = 0


    hour = int(time) #get hour
    minute = round((time - hour)*60) #minutes = 60(minute) * 0.() （the decimal fraction）
    brevet_time = arrow.get(brevet_start_time) #brevet_time store the start time use arrow
    close = brevet_time.shift(hours = hour, minutes = minute) # change the time, get close time

    return close.isoformat()
